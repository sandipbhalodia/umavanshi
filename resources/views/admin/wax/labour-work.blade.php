@extends('admin.layouts.master')

@push('stylesheets')
@endpush

@section('content')
@parent
<div class="page-heading">
    <h3>
        {{__('Wax work')}}
    </h3>    
</div>
<div class="wrapper">    
    <div class="row">
                <div class="col-md-4">
                    <section class="">                            
                            <div class="">                                
                                <p>                                    
                                    <a href="{{route('wax_work_new_entry',['labour_id' => $labour_id])}}" class="btn btn-info" type="button">{{__('New Entry')}}</a>
                                    <button class="btn btn-default" type="button">Default button</button>
                                </p>                                
                            </div>                        
                    </section>
                </div>
                
            </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">                
                <div class="panel-body">
                    <section id="unseen">
                        @if(count($works) > 0)
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th width="5%">{{__('ID')}}</th>
                                    <th width="8%">{{__('Given Date')}}</th>
                                    <th width="5%">{{__('Given Pieces')}}</th>
                                    <th width="8%">{{__('Given Weight')}}</th>
                                    <th width="8%">{{__('Return Date')}}</th>
                                    <th width="8%">{{__('Return Weight')}}</th>                                    
                                    <th width="8%">{{__('Remaining Weight')}}</th>
                                    <th width="8%">{{__('Remaining Weight Status')}}</th>
                                    <th width="8%">{{__('Unit Price')}}</th>
                                    <th width="8%">{{__('Total Price')}}</th>
                                    <th width="15%">{{__('Detail')}}</th>         
                                    <th width="11%">{{__('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($works as $work)
                                <tr>
                                    <td>{{$work['id']}}</td>
                                    <td>{{Carbon\Carbon::parse($work['given_date'])->format('d/m/Y')}}</td>
                                    <td>{{$work['given_pieces']}}</td>
                                    <td>{{$work['given_weight']}}</td>
                                    <td>{{Carbon\Carbon::parse($work['return_date'])->format('d/m/Y')}}</td>
                                    <td>{{$work['return_weight']}}</td>                                    
                                    <td>{{$work['remaining_weight']}}</td>
                                    <td>{{__($work['remaining_weight_status'])}}</td>
                                    <td>{{$work['unit_price']}}</td>
                                    <td>{{$work['total_price']}}</td>
                                    <td>{{$work['detail']}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">{{__('Action')}}<span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu">
                                                @if(!$work['is_completed'])
                                                    <li><a href="{{route('wax_work_complete_entry',['entry_id' => $work['id']])}}" alt="{{__('Complete')}}" title="{{__('Complete')}}">{{__('Complete')}}</a></li>
                                                @endif
                                                <li><a href="{{route('wax_work_edit_entry',['entry_id' => $work['id']])}}" alt="{{__('Edit Entry')}}" title="{{__('Edit Entry')}}">{{__('Edit Entry')}}</a></li>
                                            </ul>
                                        </div><!-- /btn-group -->
                                    </td>
                                </tr>                        
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        
                        @endif
                    </section>
                </div>
            </section>                        
        </div>
    </div>    
</div>
@endsection

@push('scripts')
@endpush