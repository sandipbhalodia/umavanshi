@extends('admin.layouts.master')

@push('stylesheets')
@endpush

@section('content')
@parent
<div class="page-heading">
    <h3>
        Wax Labour List
    </h3>    
</div>
<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Responsive table
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <section id="unseen">
                        @if(count($labours) > 0)
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($labours as $labour)
                                <tr>
                                    <td>{{$labour['id']}}</td>
                                    <td>
                                        <a href="{{route('wax_work_list',['labour_id' => $labour['id']])}}" alt="" title="">{{$labour['name']}}</a>
                                        
                                    </td>
                                </tr>                        
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        
                        @endif
                    </section>
                </div>
            </section>                        
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush