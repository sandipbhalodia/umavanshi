<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['middleware' => ['web'], 'namespace' => 'Admin'], function () {
    // route to show the login form
    Route::get('login', array('uses' => 'HomeController@showLogin'))->name('show_login');

// route to process the form
    Route::post('login', array('uses' => 'HomeController@doLogin'))->name('do_login');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin'], function () {
    Route::get('/', 'HomeController@index')->name('homepage');
    Route::get('/logout', 'HomeController@logout')->name('logout');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'wax'], function () {
    Route::get('/', 'WaxController@index')->name('wax_labour_list');
    Route::get('labour-work/{labour_id}', 'WaxController@labourWork')->name('wax_work_list');
    Route::get('wax-work-new-entry/{labour_id}', 'WaxController@newWorkEntry')->name('wax_work_new_entry');
    Route::post('wax-work-new-entry/{labour_id}', 'WaxController@createWorkEntry')->name('wax_work_create_entry');
    Route::get('wax-work-complete-entry/{entry_id}', 'WaxController@showComplete')->name('wax_work_complete_entry');
    Route::post('wax-work-complete-entry/{entry_id}', 'WaxController@doComplete')->name('wax_work_complete_entry');
    Route::get('wax-work-edit-entry/{entry_id}', 'WaxController@editEntry')->name('wax_work_edit_entry');
    Route::post('wax-work-edit-entry/{entry_id}', 'WaxController@updateEntry')->name('wax_work_edit_entry');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'casting'], function () {
    Route::get('/', 'CastingController@index')->name('casting_labour_list');
    Route::get('labour-work/{id}', 'CastingController@labourWork')->name('casting_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'nachak'], function () {
    Route::get('/', 'NachakController@index')->name('nachak_labour_list');
    Route::get('labour-work/{id}', 'NachakController@labourWork')->name('nachak_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'jaboro'], function () {
    Route::get('/', 'JaboroController@index')->name('jaboro_labour_list');
    Route::get('labour-work/{id}', 'JaboroController@labourWork')->name('jaboro_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'drum'], function () {
    Route::get('/', 'DrumController@index')->name('drum_labour_list');
    Route::get('labour-work/{id}', 'DrumController@labourWork')->name('drum_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'rev'], function () {
    Route::get('/', 'RevController@index')->name('rev_labour_list');
    Route::get('labour-work/{id}', 'RevController@labourWork')->name('rev_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'dull'], function () {
    Route::get('/', 'DullController@index')->name('dull_labour_list');
    Route::get('labour-work/{id}', 'DullController@labourWork')->name('dull_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'chholkam'], function () {
    Route::get('/', 'ChholkamController@index')->name('chholkam_labour_list');
    Route::get('labour-work/{id}', 'ChholkamController@labourWork')->name('chholkam_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'gold'], function () {
    Route::get('/', 'GoldController@index')->name('gold_labour_list');
    Route::get('labour-work/{id}', 'GoldController@labourWork')->name('gold_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'dimond'], function () {
    Route::get('/', 'DimondController@index')->name('dimond_labour_list');
    Route::get('labour-work/{id}', 'DimondController@labourWork')->name('dimond_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'mino'], function () {
    Route::get('/', 'MinoController@index')->name('mino_labour_list');
    Route::get('labour-work/{id}', 'MinoController@labourWork')->name('mino_work_list');
});

Route::group(['middleware' => ['web', 'admin'], 'namespace' => 'Admin', 'prefix' => 'packing'], function () {
    Route::get('/', 'PackingController@index')->name('packing_labour_list');
    Route::get('labour-work/{id}', 'PackingController@labourWork')->name('packing_work_list');
});

