<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WaxWork extends Model
{
    protected $table   = 'wax_work';
    public static function getLabourWork($labour_id){
        return WaxWork::query()->where('wax_labour_id',$labour_id)->orderBy('created_at','desc')->get()->toArray();
    }
}
