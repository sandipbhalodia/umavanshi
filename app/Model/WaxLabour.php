<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WaxLabour extends Model
{
    protected $table   = 'wax_labour';
    public static function getLabourList(){
        return WaxLabour::query()->where('is_active',1)->get()->toArray();
    }
}
