<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\WaxLabour;
use App\Model\WaxWork;
use Carbon\Carbon;
use Validator;

class WaxController extends Controller {

    public function index() {
        $labours = WaxLabour::getLabourList();
        return view('admin.wax.index', array('labours' => $labours));
    }

    public function labourWork($labour_id, Request $request) {
//        echo $locale = \App::getLocale();exit;
        $works = WaxWork::getLabourWork($labour_id);
        return view('admin.wax.labour-work', array('works' => $works, 'labour_id' => $labour_id));
    }

    public function newWorkEntry($labour_id, Request $request) {
        return view('admin.wax.new-work', array('labour_id' => $labour_id));
    }

    public function createWorkEntry($labour_id, Request $request) {
        $this->validate($request, [
            'given_date' => 'required|date_format:d/m/Y',
            'given_pieces' => 'numeric',
            'given_weight' => 'numeric',
            'unit_price' => 'numeric',
            'detail' => 'required',
        ]);



        $wax_entry = new WaxWork;
        $wax_entry->wax_labour_id = $labour_id;
        $given_date_carbon = Carbon::createFromFormat('d/m/Y', $request->input('given_date'));
        $wax_entry->given_date = $given_date_carbon->format('Y-m-d');
        $wax_entry->given_pieces = $request->input('given_pieces');
        $wax_entry->given_weight = $request->input('given_weight');
        $wax_entry->unit_price = $request->input('unit_price');
        $wax_entry->detail = $request->input('detail');
        $wax_entry->save();

        return redirect()->route('wax_work_list', ['labour_id' => $labour_id]);
    }

    public function showComplete($entry_id, Request $request) {
        $entry = WaxWork::find($entry_id);
        return view('admin.wax.complete-work', ['entry' => $entry]);
    }

    public function doComplete($entry_id, Request $request) {
        $this->validate($request, [
            'return_date' => 'required|date_format:d/m/Y',
            'return_pieces' => 'numeric',
            'return_weight' => 'numeric',
            'remaining_weight' => 'numeric',
            'remaining_weight_status' => 'required',
            'total_price' => 'numeric',
            'detail' => 'required',
        ]);



        $wax_entry = WaxWork::find($entry_id);
        $return_date_carbon = Carbon::createFromFormat('d/m/Y', $request->input('return_date'));
        $wax_entry->return_date = $return_date_carbon->format('Y-m-d');
        $wax_entry->return_pieces = $request->input('return_pieces');
        $wax_entry->return_weight = $request->input('return_weight');
        $wax_entry->remaining_weight = $request->input('remaining_weight');
        $wax_entry->remaining_weight_status = $request->input('remaining_weight_status');
        $wax_entry->total_price = $request->input('total_price');
        $wax_entry->detail = $request->input('detail');
        $wax_entry->is_completed = '1';
        $wax_entry->save();

        if ($request->input('remaining_weight_status') == 'Returned To Labour') {
            $labour = WaxLabour::find($wax_entry->wax_labour_id);
            $labour->wax_weight_stock = ($labour->wax_weight_stock + $request->input('remaining_weight'));
            $labour->save();
        }

        return redirect()->route('wax_work_list', ['labour_id' => $wax_entry->wax_labour_id]);
    }

    public function editEntry($entry_id, Request $request) {
        $entry = WaxWork::find($entry_id);
        return view('admin.wax.edit-work', ['entry' => $entry]);
    }

    public function updateEntry($entry_id, Request $request) {
        $this->validate($request, [
            'given_date' => 'required|date_format:d/m/Y',
            'given_pieces' => 'numeric',
            'given_weight' => 'numeric',
            'unit_price' => 'numeric',            
            'return_date' => 'required|date_format:d/m/Y',
            'return_pieces' => 'numeric',
            'return_weight' => 'numeric',
            'remaining_weight' => 'numeric',
            'remaining_weight_status' => 'required',
            'total_price' => 'numeric',
            'detail' => 'required',
        ]);



        $wax_entry = WaxWork::find($entry_id);
        
        $given_date_carbon = Carbon::createFromFormat('d/m/Y', $request->input('given_date'));
        $wax_entry->given_date = $given_date_carbon->format('Y-m-d');
        $wax_entry->given_pieces = $request->input('given_pieces');
        $wax_entry->given_weight = $request->input('given_weight');
        $wax_entry->unit_price = $request->input('unit_price');
        
        $return_date_carbon = Carbon::createFromFormat('d/m/Y', $request->input('return_date'));
        $wax_entry->return_date = $return_date_carbon->format('Y-m-d');
        $wax_entry->return_pieces = $request->input('return_pieces');
        $wax_entry->return_weight = $request->input('return_weight');
        $wax_entry->remaining_weight = $request->input('remaining_weight');
        $wax_entry->remaining_weight_status = $request->input('remaining_weight_status');
        $wax_entry->total_price = $request->input('total_price');
        $wax_entry->detail = $request->input('detail');
        $wax_entry->is_completed = '1';
        $wax_entry->save();

//        if ($request->input('remaining_weight_status') == 'Returned To Labour') {
//            $labour = WaxLabour::find($wax_entry->wax_labour_id);
//            $labour->wax_weight_stock = ($labour->wax_weight_stock + $request->input('remaining_weight'));
//            $labour->save();
//        }        
        return redirect()->route('wax_work_list', ['labour_id' => $wax_entry->wax_labour_id]);
    }

}
