<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWax extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('wax_labour')) {
            Schema::create('wax_labour', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 100);
                $table->boolean('is_active')->default(1);
                $table->decimal('wax_weight_stock', 10, 2)->default(0.0);
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('wax_work')) {
            Schema::create('wax_work', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('wax_labour_id')->unsigned();
                
                $table->date('given_date');
                $table->integer('given_pieces')->nullable();                
                $table->decimal('given_weight', 10, 2)->nullable();
                
                $table->date('return_date')->nullable();
                $table->integer('return_pieces')->nullable();                
                $table->decimal('return_weight', 10, 2)->nullable();                                
                $table->decimal('remaining_weight', 10, 2)->nullable();
                $table->enum('remaining_weight_status', ['Returned To Labour', 'Deposited Back'])->default('Returned To Labour');
                
                $table->decimal('unit_price', 10, 2)->nullable();
                $table->decimal('total_price', 10, 2)->nullable();
                $table->text('detail')->nullable();
                $table->boolean('is_completed')->default(0);
                $table->timestamps();

                $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
