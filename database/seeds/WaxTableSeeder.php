<?php

use Illuminate\Database\Seeder;

class WaxTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $wax_labour_id_1 = DB::table('wax_labour')->insertGetId([
            'name' => 'Babubhai'
        ]);
        
        $wax_labour_id_2 = DB::table('wax_labour')->insertGetId([
            'name' => 'Dineshbhai'
        ]);
        $wax_labour_id_3 = DB::table('wax_labour')->insertGetId([
            'name' => 'Rahulbhai'
        ]);
        
        DB::table('wax_work')->insert([
            'wax_labour_id' => $wax_labour_id_1,
            'given_date' => date("Y-m-d"),
            'given_pieces' => 1500,
            'given_weight' => 3600.50,
            'return_date' => date("Y-m-d", strtotime('tomorrow')),
            'return_pieces' => 1500,
            'return_weight' => 2000.50,            
            'remaining_weight' => 1600.00,
            'remaining_weight_status' => 'Returned To Labour',
            'unit_price' => 0.125,
            'total_price' => (1500 * 0.125),
            'detail' => 'Ramola',
            'is_completed' => 1
        ]);
        
        DB::table('wax_work')->insert([
            'wax_labour_id' => $wax_labour_id_2,
            'given_date' => date("Y-m-d"),
            'given_pieces' => 2000,            
            'given_weight' => 4000.00,
            'return_date' => date("Y-m-d", strtotime('tomorrow')),
            'return_pieces' => 2000,
            'return_weight' => 3000.00,            
            'remaining_weight' => 1000.00,
            'remaining_weight_status' => 'Returned To Labour',
            'unit_price' => 0.125,
            'total_price' => (2000 * 0.125),
            'detail' => 'Ramola',
            'is_completed' => 1
        ]);
        
        DB::table('wax_work')->insert([
            'wax_labour_id' => $wax_labour_id_3,
            'given_date' => date("Y-m-d"),
            'given_pieces' => 3000,            
            'given_weight' => 5000.00,
            'return_date' => date("Y-m-d", strtotime('tomorrow')),
            'return_pieces' => 3000,
            'return_weight' => 4000.00,            
            'remaining_weight' => 1000.00,
            'remaining_weight_status' => 'Returned To Labour',
            'unit_price' => 0.125,
            'total_price' => (3000 * 0.125),
            'detail' => 'Ramola',
            'is_completed' => 1
        ]);
    }

}
